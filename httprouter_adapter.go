package buckaroo

import(
	"github.com/julienschmidt/httprouter"
	"golang.org/x/net/context"
	"net/http"
)
type key int

const (
	paramsKey key = iota
)

// Params returns the httprouter.Params of a  request or panics if it does not exist.
// For example, with the path /v2/papers/:page
// use buckaroo.Params(ctx) to get the httprouter.Params. Then use the httprouter.Params "ByName()" method to access the
// :page variable.
func Params(ctx context.Context) httprouter.Params {
	params, ok := ctx.Value(paramsKey).(httprouter.Params)
	if !ok {
		panic("No params object for request. This should never happen.")
	}
	return params
}

// Used internally to save httprouter.Params to the context
func newContextWithParams(ctx context.Context, params httprouter.Params) context.Context {
	return context.WithValue(ctx, paramsKey, params)
}
// Wraps the middleware chain and its handler and returns a httprouter.Handle
func (l *Lariat) HandleHTTPRouter(h HandlerType) httprouter.Handle{
	wrapped := ConvertHandler(h)
	l.ctx= context.Background()
	for i:= len(l.middleware) - 1; i>=0;i--{
		wrapped=l.middleware[i].wrap(wrapped)
	}
	return func (w http.ResponseWriter, r *http.Request, params httprouter.Params){
		if len(params) > 0{
			l.ctx=newContextWithParams(l.ctx,params)
		}
		wrapped.ServeHTTPContext(l.ctx,w,r)
	}


}

