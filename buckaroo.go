package buckaroo

import (
	"net/http"
	"golang.org/x/net/context"
	"fmt"
)

// Context Handling inspired by https://joeshaw.org/net-context-and-http-handler/

//Lariat is the main middleware chaining data structure. It contains
// its own context element and the middleware chain.
type (
	Lariat struct{
		ctx context.Context
		middleware []MiddlewareFunc
	}
// HandlerType is the type of Handlers and types that buckaroo internally converts to
// ContextHandler. In order to provide an expressive API, this type is an alias for
// interface{} that is named for the purposes of documentation, however only the
// following concrete types are accepted:
// 	- types that implement http.Handler
// 	- types that implement ContextHandler
// 	- func(http.ResponseWriter, *http.Request)
// 	- func(context.Context, http.ResponseWriter, *http.Request)
	HandlerType interface {}

// MiddlewareType represents types that buckaroo can convert to Middleware.
// buckaroo will try its best to convert standard, non-context middleware.In order to provide an
// expressive API, this type is an alias for interface{} that is named for the purposes of documentation,
// The following concrete types are accepted:
// 	- MiddlewareFunc
//  - func(ContextHandler) ContextHandler
//  - func(http.Handler) http.Handler
	MiddleWareType interface {}

// buckaroos middleware type
	MiddlewareFunc func(ContextHandler) ContextHandler

// ContextHandler is like http.Handler but supports context
	ContextHandler interface{
		ServeHTTPContext(context.Context, http.ResponseWriter, *http.Request)
	}
//	Middleware interface provides wrapping functionality so that the final handler can be wrapped and used in the chain.
	Middleware interface {
		wrap(ctxhndlr ContextHandler) ContextHandler
	}
// ContextHandlerFunc is like http.HandlerFunc with context
	ContextHandlerFunc func (context.Context, http.ResponseWriter, *http.Request)

)
//Implementing buckaroo.ContextHandler on ContextHandlerFunc
func ( h ContextHandlerFunc) ServeHTTPContext(ctx context.Context, rw http.ResponseWriter, req *http.Request){
	h(ctx, rw, req)
}
// Implementing buckaroo.Middleware on MiddlewareFunc
func (h MiddlewareFunc) wrap(ctxhndlr ContextHandler) ContextHandler{
	return h(ctxhndlr)
}

// ConvertMW tries to turn a MiddlewareType into a MiddlewareFunc
func ConvertMW(mw MiddleWareType) MiddlewareFunc{
	switch x := mw.(type){
		case MiddlewareFunc:
		return x
		case func(ContextHandler) ContextHandler:
		return x
		case func(http.Handler) http.Handler:
		return wrapHTTPMiddleware(x)


	}
	panic(fmt.Errorf("unsupported middlewaretype: %T",mw))
}
//Used internally by ConvertMW(mw MiddlewareType) to convert middleware that conforms to func (http.Handler) http.Handler
func wrapHTTPMiddleware(httpMiddleware func (http.Handler) http.Handler) MiddlewareFunc{
	fn := func(ctxhndler ContextHandler) ContextHandler {
		return ContextHandlerFunc(func(c context.Context, w http.ResponseWriter, r *http.Request) {
			newFn := func(ww http.ResponseWriter, rr *http.Request) {
				ctxhndler.ServeHTTPContext(c, ww, rr)
			}

			var fn http.HandlerFunc
			fn= httpMiddleware(http.HandlerFunc(newFn)).ServeHTTP
			fn(w,r)
		})
	}
	return fn
}
// ConvertHandler tries to turn a HandlerType into a ContextHandler
func ConvertHandler(h HandlerType) ContextHandler {
	switch x := h.(type) {
		case ContextHandler:
		return x
		case func(context.Context, http.ResponseWriter, *http.Request):
		return ContextHandlerFunc(x)
		case http.Handler:
		return ContextHandlerFunc(func(_ context.Context, w http.ResponseWriter, r *http.Request) {
			x.ServeHTTP(w, r)
		})
		case func(http.ResponseWriter, *http.Request):
		return ContextHandlerFunc(func(_ context.Context, w http.ResponseWriter, r *http.Request) {
			x(w, r)
		})
		case http.HandlerFunc:
		return ContextHandlerFunc(func(_ context.Context,w http.ResponseWriter, r *http.Request){
			x(w,r)
		})
	}
	panic(fmt.Errorf("unsupported HandlerType: %T", h))
}

//Creates a new Lariat struct
func New (m ...MiddleWareType) (Lariat) {
	l := Lariat{}
	for _, i := range m{
		l.middleware = append(l.middleware,ConvertMW(i))
	}
	return l
}
//Creates a new Lariat struct and appends the middleware
func (l Lariat) Append(m ...MiddleWareType) (Lariat){
	for _, i := range m {
		l.middleware=append(l.middleware, ConvertMW(i))
	}
	return  l
}

//Sets a predefined context.Context
func (l Lariat) SetContext(ctx context.Context)Lariat{
	l.ctx = ctx
	return l
}
// Wraps the middleware chain and its handler and returns a http.Handler
func (l *Lariat) Handle(h HandlerType) http.Handler {
	wrapped := ConvertHandler(h)
	if l.ctx == nil {
		l.ctx = context.Background()
	}
	for i:= len(l.middleware) - 1; i>=0;i--{
		wrapped=l.middleware[i].wrap(wrapped)
	}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		wrapped.ServeHTTPContext(l.ctx,w,r)
	})

}

