#Buckaroo

Buckaroo is a context aware HTTP middleware chaining utility utilizing go’s net/context

## Why?
Buckaroo attempts to make context aware middleware chaining idiomatic and independent of web frameworks.
It creates just enough of a middleware/context framework to be reusable and modular.

### Usage
Buckaroo relies on a simple modification of go’s native http.Handler.
The buckaroo.Contexthandler’s interface is as follows:

```go
ContextHandler interface{
            ServeHTTPContext(context.Context, http.ResponseWriter, *http.Request)
                }
    }
```
Thus, buckaroo.ContextHandlerFunc is simply:

```go
ContextHandlerFunc func (context.Context, http.ResponseWriter, *http.Request)
```
#### Buckaroo takes the following middleware types:
    - buckaroo.MiddlewareFunc
    - func(buckaroo.ContextHandler) buckaroo.ContextHandler
    - func(http.Handler) http.Handler

#### Buckaroo also takes the following handler types:
    - types that implement http.Handler
    - types that implement ContextHandler
    - func(http.ResponseWriter, *http.Request)
    - func(context.Context, http.ResponseWriter, *http.Request)

### Example
Using go’s stock router:
```go
package main

import(
	"bitbucket.org/mreisch/buckaroo"
	"net/http"
	"fmt"
	"time"
	"log"
	"golang.org/x/net/context"
)

type key int
const requestIDKey key = 0

func newContextWithRequestID(ctx context.Context, req *http.Request) context.Context {
	return context.WithValue(ctx, requestIDKey, "djksdfk345")
}

func requestIDFromContext(ctx context.Context) string {
	return ctx.Value(requestIDKey).(string)
}

func loggingHandler(next http.Handler) http.Handler{
	fn := func(w http.ResponseWriter, r *http.Request){
		t1 := time.Now()
		next.ServeHTTP(w, r)
		t2 := time.Now()
		log.Printf("[%s] %q %v\n", r.Method,r.URL.String(),t2.Sub(t1))
	}
	return http.HandlerFunc(fn)
}
func middleware(h buckaroo.ContextHandler) buckaroo.ContextHandler {
	return buckaroo.ContextHandlerFunc(func(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
		ctx = newContextWithRequestID(ctx, req)
		h.ServeHTTPContext(ctx, rw, req)
	})
}
func handler(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
	reqID := requestIDFromContext(ctx)
	fmt.Fprintf(rw, "Hello request ID %s\n", reqID)
}

func main(){
	b := buckaroo.New(loggingHandler,middleware)
	http.Handle("/", b.Handle(handler))
	http.ListenAndServe(":8080",nil)

}
```
### Support for other routers
Because buckaroo returns an http.Handle it should work with all 3rd party routers. For convenience sake buckaroo comes with an adapter for
julienschmidt/httprouter.

Example:
```go
package main

import(
	"bitbucket.org/mreisch/buckaroo"
	"net/http"
	"fmt"
	"time"
	"log"
	"golang.org/x/net/context"
	"github.com/julienschmidt/httprouter"
)

type key int
const requestIDKey key = 0

func newContextWithRequestID(ctx context.Context, req *http.Request) context.Context {
	return context.WithValue(ctx, requestIDKey, "djksdfk345")
}

func requestIDFromContext(ctx context.Context) string {
	return ctx.Value(requestIDKey).(string)
}


func loggingHandler(next http.Handler) http.Handler{
	fn := func(w http.ResponseWriter, r *http.Request){
		t1 := time.Now()
		next.ServeHTTP(w, r)
		t2 := time.Now()
		log.Printf("[%s] %q %v\n", r.Method,r.URL.String(),t2.Sub(t1))
	}
	return http.HandlerFunc(fn)
}
func middleware(h buckaroo.ContextHandler) buckaroo.ContextHandler {
	return buckaroo.ContextHandlerFunc(func(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
		ctx = newContextWithRequestID(ctx, req)
		h.ServeHTTPContext(ctx, rw, req)
	})
}
func handler(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
	reqID := requestIDFromContext(ctx)
	fmt.Fprintf(rw, "Hello request ID %s\n", reqID)
}

func HelloCTX(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
	reqID := requestIDFromContext(ctx)
	params := buckaroo.Params(ctx)
	fmt.Fprintf(rw, "hello, %s!\n", params.ByName("name"))
	fmt.Fprintf(rw, "Your id is %s\n", reqID)
}

func main(){
	b := buckaroo.New(loggingHandler,middleware)
	router:=httprouter.New()
	router.GET("/hello/:name",b.HandleHTTPRouter(HelloCTX))
	log.Fatal(http.ListenAndServe(":8080", router))

}
```
